﻿# Redactor Word Count

## Description

A simple Javascript plugin for the [Redactor WYSIWYG editor](http://imperavi.com/redactor/ "Redactor WYSIWYG editor") which displays the number of words in the editor content.

## Installation

* Copy redactor.wordcount.min.js and wordcount.css into your project

* Link redactor.wordcount.min.js and wordcount.css in the header of the page containing Redactor

* Add wordCount to the list of available plugins when you initialize Redactor:
  >  $('#redactor').redactor({
  >      plugins: ['wordcount']
  >  });